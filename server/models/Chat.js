import mongoose from "mongoose";
import User from "./User.js";
import Message from "./Message.js";
const Chat = new mongoose.Schema({
  user: { type: Object, ref: "User", default: User },
  message: { type: Object, ref: "Message", default: Message },
});

export default mongoose.model("Chat", Chat);

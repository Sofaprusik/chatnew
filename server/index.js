import exspress from "express";
import mongoose from "mongoose";
import cors from "cors";
import authRouter from "./routes/authRouter.js";
import userRouter from "./routes/userRouter.js";
import messageRouter from "./routes/messageRouter.js";
import Role from "./models/Role.js";
const PORT = "5000";
const DB_URL = `mongodb+srv://admin:admin@cluster0.vp5tr.mongodb.net/myFirstDatabase?retryWrites=true`;
const app = exspress();
app.use(cors());
app.use(exspress.json());
app.use("/api", authRouter);
app.use("/api", userRouter);
app.use("/api", messageRouter);
app.get("/api/role", async (req, res) => {
  const role = await Role.find();
  res.json(role);
});
app.post("/api/role", async (req, res) => {
  const { value } = req.body;
  const role = await Role.create({ value });
  res.json(role);
});
app.get("/", (req, res) => {
  res.status(200).json("Server started");
});

async function startApp() {
  try {
    await mongoose.connect(DB_URL, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    });
    app.listen(PORT, () => console.log("Server started on port " + PORT));
  } catch (e) {
    console.log(e);
  }
}
startApp();

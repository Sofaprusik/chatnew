import Router from "express";
import messageService from "../services/messageService.js";
const router = new Router();
router.post("/", messageService.create);
router.get("/", messageService.getAll);

export default router;

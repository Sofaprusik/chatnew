import Router from "express";
import userService from "../services/userService.js";
const router = new Router();
router.post("/users", userService.create);
router.get("/users", userService.getAll);
router.get("/users/:id", userService.getOne);
router.put("/users", userService.update);
router.delete("/users/:id", userService.delete);

export default router;

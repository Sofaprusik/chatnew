import Router from "express";
import authService from "../services/authService.js";
const router = new Router();
router.post("/login", authService.login);
router.get("/login", (req, res) => {
  res.status(200).json("og");
});
export default router;

import authRepository from "../repositories/authRepository.js";
class authService {
  async login(req, res) {
    try {
      const user = await authRepository.login(req.body);
      return res.json(user);
    } catch (e) {
      res.status(500).json(e);
    }
  }
}

export default new authService();

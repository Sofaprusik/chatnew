import messageRepository from "../repositories/messageRepository.js";

class messageService {
  async create(req, res) {
    try {
      const message = await messageRepository.create(req.body);
      res.json(message);
    } catch (e) {
      res.status(500).json(e);
    }
  }
  async getAll(req, res) {
    try {
      const message = await messageRepository.getAll();
      return res.json(message);
    } catch (e) {
      res.status(500).json(e);
    }
  }
  //   async getOne(req, res) {
  //     try {
  //       const user = await userRepository.getOne(req.params.id);
  //       return res.json(user);
  //     } catch (e) {
  //       res.status(500).json(e);
  //     }
  //   }
  //   async update(req, res) {
  //     try {
  //       const updatedUser = await userRepository.update(req.body);
  //       return res.json(updatedUser);
  //     } catch (e) {
  //       res.status(500).json(e.message);
  //     }
  //   }
  //   async delete(req, res) {
  //     try {
  //       const user = await userRepository.delete(req.params.id);
  //       return res.json(user);
  //     } catch (e) {
  //       res.status(500).json(e);
  //     }
  //   }
}
export default new messageService();

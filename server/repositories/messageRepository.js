import Message from "../models/Message.js";
class messageRepository {
  async create(body) {
    const { text, user } = body;
    user.toString();
    const createdMessage = await Message.create({ text, user });
    return createdMessage;
  }
  async getAll() {
    const message = await Message.find().populate({
      path: "user",
      select: ["username", "avatar"],
    });
    return message;
  }
}
export default new messageRepository();

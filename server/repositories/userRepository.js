import User from "../models/User.js";
import Role from "../models/Role.js";
class userRepository {
  async create(body) {
    const { username, password, avatar } = body;
    try {
      const userRole = await Role.findOne({ value: "USER" });
      const createdUser = new User({
        username,
        password,
        avatar,
        roles: [userRole.value],
      });
      await createdUser.save();
      return createdUser;
    } catch (e) {
      console.log(e);
    }
  }
  async getAll() {
    const users = await User.find();
    return users;
  }
  async getOne(id) {
    if (!id) {
      throw new Error("ID not found");
    }
    const user = await User.findById(id);
    return user;
  }

  async update(user) {
    if (!user._id) {
      throw new Error("ID not found");
    }
    const updatedUser = await User.findByIdAndUpdate(user._id, user, {
      new: true,
    });
    return updatedUser;
  }

  async delete(id) {
    if (!id) {
      throw new Error("ID not found");
    }
    const user = await User.findByIdAndDelete(id);
    return user;
  }
}
export default new userRepository();

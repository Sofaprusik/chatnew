import User from "../models/User.js";

class authRepository {
  async login(body) {
    const { username } = body;
    const user = await User.findOne({ username });
    if (!user) {
      throw `user ${username} not found`;
    }
    return user;
  }
}
export default new authRepository();

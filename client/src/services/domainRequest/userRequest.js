import { post } from "../requestHelper";
import { get } from "../requestHelper";
import { getAll } from "../requestHelper";
import { put } from "../requestHelper";
import { deleteReq } from "../requestHelper";
const entity = "users";

export const createUser = async (body) => {
  return await post(entity, body);
};
export const getAllUsers = async () => {
  return await getAll(entity);
};
export const getUser = async (id) => {
  return await get(entity, id);
};
export const updateUser = async (id, body) => {
  return await put(entity, id, body);
};
export const deleteUser = async (id) => {
  return await deleteReq(entity, id);
};

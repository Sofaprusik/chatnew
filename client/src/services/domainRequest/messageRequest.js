import { getAll } from "../requestHelper";
const entity = "";

export const getAllMessage = async () => {
  return await getAll(entity);
};

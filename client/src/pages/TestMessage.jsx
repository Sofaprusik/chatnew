import React, { useState, useEffect } from "react";
const TestMessage = ({ data }) => {
  return (
    <>
      {data.map((e) => (
        <>
          <div>{e.text}</div>
          <li>{e.user["username"]}</li>
          <li>{e.user["avatar"]}</li>
        </>
      ))}
    </>
  );
};

export default TestMessage;

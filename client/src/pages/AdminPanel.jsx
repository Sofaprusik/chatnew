import React, { useState, useEffect } from "react";
import { Link, Route, Switch } from "react-router-dom";
import Button from "react-bootstrap/Button";
import { deleteUser } from "../services/domainRequest/userRequest";
import { getAllUsers } from "../services/domainRequest/userRequest";
import Preloader from "../components/Preloader";

// import { useDispatch, useSelector } from "react-redux";
// import { getUsers, deleteUser } from "../slices/users";

const AdminPanel = () => {
  const [users, setUser] = useState([]);
  const [loading, setLoading] = useState(false);
  // const users = useSelector((state) => state.users);
  // console.log("user", users);
  // const dispatch = useDispatch();
  // const initFetch = useCallback(() => {
  //   dispatch(getUsers());
  // }, [dispatch]);

  // useEffect(() => {
  //   initFetch();
  // }, [initFetch]);

  useEffect(() => {
    loadUsers();
  }, []);

  const loadUsers = async () => {
    const res = await getAllUsers();
    console.log("users", res);
    setUser(res);
    setTimeout(() => setLoading(true), 1000);
  };

  const deleteUsers = async (id) => {
    const res = await deleteUser(id);
    loadUsers();
    return res;
  };

  return (
    <>
      {loading ? (
        <div className="container">
          <div className="py-4 ">
            <div className="admin-panel">
              <h2>Admin Panel</h2>
              <Button variant="primary">
                <Link to="/users/add/" className="add-user">
                  Add User
                </Link>
              </Button>
            </div>
            <table className="table border shadow admin-panel-text">
              <thead>
                <tr>
                  <th scope="col">№</th>
                  <th scope="col"></th>
                  <th>User Name</th>
                  <th>Avatar</th>
                </tr>
              </thead>
              <tbody className="thead-dark">
                {users.map((user, index) => (
                  <tr>
                    <th scope="row">{index + 1}</th>
                    <td>{user.username}</td>
                    <td>
                      <img src={user.avatar} className="user-avatar"></img>
                    </td>
                    <td>
                      <Link
                        className="btn btn-outline-primary mr-2"
                        to={`/users/edit/${user._id}`}
                      >
                        Edit
                      </Link>
                      <div
                        className="btn btn-danger edit-admin"
                        onClick={() => deleteUsers(user._id)}
                      >
                        Delete
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      ) : (
        <Preloader></Preloader>
      )}
    </>
  );
};
export default AdminPanel;

import React, { useEffect, useState } from "react";
import TestMessage from "./TestMessage";
import { getAllMessage } from "../services/domainRequest/messageRequest";

const Chat = () => {
  const [store, setStore] = useState([]);
  const getData = async () => {
    const res = await getAllMessage();
    setStore(res);
    console.log("data", res);
  };
  useEffect(() => {
    getData();
  }, []);

  return (
    <>{store ? <TestMessage data={store}></TestMessage> : <p>Not faund</p>}</>
  );
};

export default Chat;

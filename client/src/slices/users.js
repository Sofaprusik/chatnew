import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import userRequest from "../services/domainRequest/userRequest";
import { deleteUser } from "../services/domainRequest/userRequest";
import { getAllUsers } from "../services/domainRequest/userRequest";
import { createUser } from "../services/domainRequest/userRequest";
import { updateUser } from "../services/domainRequest/userRequest";

const initialState = [];

export const createUsers = createAsyncThunk(
  "users/createUser",
  async ({ username, login, avatar }) => {
    const res = await createUser({ username, login, avatar });
    return res.data;
  }
);
export const getUsers = createAsyncThunk("users/getAll", async () => {
  const res = await getAllUsers();
  return res.data;
});
export const updateUsers = createAsyncThunk(
  "users/update",
  async ({ id, data }) => {
    const res = await updateUser(id, data);
    return res.data;
  }
);
export const deleteUsers = createAsyncThunk("users/delete", async ({ id }) => {
  await deleteUser(id);
  return { id };
});

const userSlice = createSlice({
  name: "user",
  initialState,
  extraReducers: {
    [createUsers.fulfilled]: (state, action) => {
      state.push(action.payload);
    },
    [getUsers.fulfilled]: (state, action) => {
      return [{ ...action.payload }];
    },
    [updateUsers.fulfilled]: (state, action) => {
      const index = state.findIndex((user) => user.id === action.payload.id);
      state[index] = {
        ...state[index],
        ...action.payload,
      };
    },
    [deleteUsers.fulfilled]: (state, action) => {
      let index = state.findIndex(({ id }) => id === action.payload.id);
      state.splice(index, 1);
    },
  },
});

const { reducer } = userSlice;
export default reducer;

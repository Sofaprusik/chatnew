import React from "react";
import AdminPanel from "./pages/AdminPanel";
import { Link, Redirect, Route, Switch } from "react-router-dom";
import NavBar from "./components/admin/NavBar";
import Login from "./pages/Login";
import Chat from "./pages/Chat";
import EditUser from "./components/admin/EditUser";
import AddUser from "./components/admin/AddUser";

function App() {
  return (
    <>
      <NavBar></NavBar>
      <Switch>
        <Route path="/login" component={Login}></Route>
        <Route path="/users/edit" component={EditUser}></Route>
        <Route path="/users/add" component={AddUser}></Route>
        <Route path="/users" component={AdminPanel}></Route>{" "}
        <Route path="/" exact component={Chat}></Route>
        <Redirect to="/users"></Redirect>
      </Switch>
    </>
  );
}

export default App;

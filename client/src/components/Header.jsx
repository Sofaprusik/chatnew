import React from "react";
import Icon from "./Icon";
const Header = ({ allMessages, messageDateLast }) => {
  return (
    <>
      <div className="header">
        <div className="header__inner">
          <div className="header-title">Chat</div>
          <div className="header-users-count">
            <span className="header-text">Online</span>
            <Icon iconClassName={"fas"} icon={"user"}></Icon>
            <span className="header-text">6</span>
          </div>
          <div className="header-messages-count">
            <Icon iconClassName={"fas"} icon={"envelope"}></Icon>
            <span className="header-text"> {allMessages}</span>
          </div>
          <div className="header-last-message-date">
            <Icon iconClassName={"fas"} icon={"calendar-day"}></Icon>
            <span className="header-text"> {messageDateLast}</span>
          </div>
        </div>
      </div>
    </>
  );
};

export default Header;

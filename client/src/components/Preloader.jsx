import React from "react";
const Preloader = () => {
  return (
    <div>
      <div className="preloader">
        <div className="lds-dual-ring"></div>
      </div>
    </div>
  );
};

export default Preloader;

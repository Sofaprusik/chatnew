import React from "react";
import Icon from "./Icon";
const OwnMessage = ({ value, deleteMessage, editMessage }) => {
  const handleDelete = (id) => {
    deleteMessage(id);
  };

  return (
    <>
      <div className="own-message">
        <div className="message">
          <div className="item">
            <span className="message-text">{value.text} </span>
          </div>
          <div className="item">
            <span className="message-time">{value.date}</span>
            <div>
              <button onClick={() => handleDelete(value.id)}>
                <Icon iconClassName={"fas"} icon={"trash"}></Icon>
              </button>
              <button onClick={() => editMessage(value)}>
                <Icon iconClassName={"fas"} icon={"edit"}></Icon>
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default OwnMessage;

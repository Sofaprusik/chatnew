import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fab } from "@fortawesome/free-brands-svg-icons";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { far } from "@fortawesome/free-regular-svg-icons";
library.add(fab, fas, far);
const Icon = ({ icon, iconClassName }) => {
  return (
    <>
      <FontAwesomeIcon className="icon active" icon={[iconClassName, icon]} />
    </>
  );
};

export default Icon;

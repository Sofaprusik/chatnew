import React, { useState, useEffect } from "react";
import { updateUser } from "../../services/domainRequest/userRequest";
import { getUser } from "../../services/domainRequest/userRequest";
import { useHistory, useParams } from "react-router-dom";

const EditUser = () => {
  let history = useHistory();
  const { _id } = useParams();
  const [user, setUser] = useState({
    username: "",
    login: "",
    avatar: "",
  });

  const { username, password, avatar } = user;

  useEffect(() => {
    loadUser();
  }, []);

  const onSubmit = async (e) => {
    e.preventDefault();
    const res = await updateUser(_id, { username, password, avatar });
    setUser(res);
    history.push("/users");
  };

  const loadUser = async () => {
    const res = await getUser(_id);
    setUser(res);
  };
  return (
    <div className="container">
      <div className="w-75 mx-auto shadow p-5">
        <h2 className="text-center mb-4">Edit A User</h2>
        <form onSubmit={(e) => onSubmit(e)}>
          <div className="form-group">
            <input
              type="text"
              className="form-control form-control-lg"
              placeholder="Enter Username"
              name="username"
              value={username}
              onChange={(e) => setUser({ ...user, username: e.target.value })}
            />
          </div>
          <div className="form-group">
            <input
              type="text"
              className="form-control form-control-lg"
              placeholder="Enter password"
              name="password"
              value={password}
              onChange={(e) => setUser({ ...user, password: e.target.value })}
            />
          </div>
          <div className="form-group">
            <input
              type="text"
              className="form-control form-control-lg"
              placeholder="Enter avatar URL"
              name="avatar"
              value={avatar}
              onChange={(e) => setUser({ ...user, avatar: e.target.value })}
            />
          </div>
          <button className="btn btn-warning btn-block">Update User</button>
        </form>
      </div>
    </div>
  );
};

export default EditUser;

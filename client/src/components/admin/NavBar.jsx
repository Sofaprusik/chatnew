import React from "react";
import { Link } from "react-router-dom";
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
const NavBar = () => {
  return (
    <>
      <Navbar bg="dark" sticky="top" variant="dark">
        <Container>
          <img
            className="user-avatar"
            src="https://resizing.flixster.com/PCEX63VBu7wVvdt9Eq-FrTI6d_4=/300x300/v1.cjs0MzYxNjtqOzE4NDk1OzEyMDA7MzQ5OzMxMQ"
          ></img>
          <Navbar.Brand href="#home">Admin</Navbar.Brand>
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">
              <Nav.Item as="li">
                <Link to="/">Chat</Link>
              </Nav.Item>
              <Nav.Item as="li">
                <Link to="/users">Users</Link>
              </Nav.Item>
            </Nav>
            <Nav>
              <Nav.Item>
                <Link to="/login">LogOut</Link>
              </Nav.Item>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
};

export default NavBar;

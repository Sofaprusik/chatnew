import React, { useState } from "react";
import { createUser } from "../../services/domainRequest/userRequest";
import { useHistory } from "react-router-dom";
import Form from "react-bootstrap/Form";
const AddUser = () => {
  let history = useHistory();
  const [user, setUser] = useState({
    username: "",
    password: "",
    avatar: "",
  });
  const { username, password, avatar } = user;
  const onSubmit = async (e) => {
    e.preventDefault();
    const res = await createUser({ username, password, avatar });
    setUser(res);
    history.push("/users");
  };
  return (
    <div className="container">
      <div className="w-75 mx-auto shadow p-5">
        <h2 className="text-center mb-4">Add A User</h2>
        <form onSubmit={(e) => onSubmit(e)}>
          <div className="form-group">
            <input
              type="text"
              className="form-control form-control-lg"
              placeholder="Enter Username"
              name="username"
              value={username}
              onChange={(e) => setUser({ ...user, username: e.target.value })}
            />
          </div>
          <div className="form-group">
            <input
              type="text"
              className="form-control form-control-lg"
              placeholder="Enter password"
              name="password"
              value={password}
              onChange={(e) => setUser({ ...user, password: e.target.value })}
            />
          </div>
          {/* <Form.Group controlId="formFile" className="mb-3">
            <Form.Label>Default file input example</Form.Label>
            <Form.Control
              type="file"
              value={avatar}
              onChange={(e) =>
                setUser({ ...user, avatar: e.target.value.toString() })
              }
            />
          </Form.Group> */}
          <div className="form-group">
            <input
              type="text"
              className="form-control form-control-lg"
              placeholder="Enter avatar URL"
              name="text"
              value={avatar}
              onChange={(e) => setUser({ ...user, avatar: e.target.value })}
            />
          </div>
          <button className="btn btn-primary btn-block">Add User</button>
        </form>
      </div>
    </div>
  );
};

export default AddUser;

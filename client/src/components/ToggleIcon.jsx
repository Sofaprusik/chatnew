import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fab } from "@fortawesome/free-brands-svg-icons";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { far } from "@fortawesome/free-regular-svg-icons";
library.add(fab, fas, far);
const ToggleIcon = ({
  active,
  handleChangeActive,
  likes,
  icon,
  iconActive,
  iconInactive,
}) => {
  return (
    <>
      {active ? (
        <>
          <FontAwesomeIcon
            className="icon active"
            icon={[iconActive, icon]}
            onClick={() => handleChangeActive(false)}
          />
          <span className="likes-count">{likes}</span>
        </>
      ) : (
        <>
          <FontAwesomeIcon
            className="icon inactive"
            icon={[iconInactive, icon]}
            onClick={() => handleChangeActive(true)}
          />
          {likes ? <span className="likes-count">{likes}</span> : <></>}
        </>
      )}
    </>
  );
};

export default ToggleIcon;

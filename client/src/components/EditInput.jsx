import React, { useState, useEffect } from "react";

const EditInput = (props) => {
  const [message, setMessage] = useState(props.currentMessage);
  useEffect(() => {
    setMessage(props.currentMessage);
  }, [props]);
  const handleSubmit = (event) => {
    event.preventDefault();
    if (!message.text) return;
    props.updateMessage(message.id, message);
  };
  return (
    <form onSubmit={handleSubmit} className="message-input">
      <span className="message-input-text">
        <input
          type="text"
          name="name"
          value={message.text}
          onChange={(e) => setMessage({ ...message, text: e.target.value })}
        />
      </span>
      <button className="message-input-button">Update</button>
    </form>
  );
};

export default EditInput;

import React, { useEffect, useState } from "react";
import MessageList from "./components/MessageList";
import OwnMessage from "./components/OwnMessage";
import MessageInput from "./components/MessageInput";
import EditInput from "./components/EditInput";
import Header from "./components/Header";
import Preloader from "./components/Preloader";
const Chat = ({ url }) => {
  const [store, setStore] = useState([]);
  const [loading, setLoading] = useState(false);
  const [allMessages, setAllMessages] = useState(0);
  const [messageDateLast, setMessageDateLast] = useState("");
  const [id, setId] = useState([]);
  const getData = async () => {
    await fetch(url)
      .then((response) => response.json())
      .then((data) => {
        setStore(data);
        // setAllMessages(data.length);
        // setMessageDateLast(
        //   new Date(data[`${data.length - 1}`].createdAt).toLocaleString()
        // );
      });
    console.log("store", store);
    setTimeout(() => setLoading(true), 1000);
  };

  //console.log(store);
  useEffect(() => {
    getData();
    store.map((e) => {
      setId([...id, e.id]);
    });
  }, []);

  const [ownMessages, setOwnMessages] = useState([]);
  const [currentMessage, setCurrentMessage] = useState({
    id: "",
    text: "",
    date: "",
  });
  const [editing, setEditing] = useState(false);
  const createMessage = (message) => {
    setOwnMessages([...ownMessages, message]);
  };
  const handleDelete = (id) => {
    setOwnMessages(ownMessages.filter((e) => e.id !== id));
  };
  const handleEdit = (message) => {
    setEditing(true);
    setCurrentMessage({
      id: message.id,
      text: message.text,
      date: message.date,
    });
  };

  const updateMessage = (id, updatedMessage) => {
    setEditing(false);
    setOwnMessages(ownMessages.map((m) => (m.id === id ? updatedMessage : m)));
  };
  useEffect(() => {
    setAllMessages(store.length + ownMessages.length);
    if (ownMessages.length > 0) {
      setMessageDateLast(ownMessages[`${ownMessages.length - 1}`].date);
    }
  }, [ownMessages]);

  return (
    <>
      <Header
        allMessages={allMessages}
        messageDateLast={messageDateLast}
      ></Header>
      <div className="content">
        {loading ? (
          <MessageList data={store}></MessageList>
        ) : (
          <Preloader></Preloader>
        )}

        {ownMessages.map((e) => (
          <OwnMessage
            key={e.id}
            deleteMessage={handleDelete}
            editMessage={handleEdit}
            value={e}
          ></OwnMessage>
        ))}
      </div>
      {editing ? (
        <EditInput
          editing={editing}
          setEditing={setEditing}
          currentMessage={currentMessage}
          updateMessage={updateMessage}
        />
      ) : (
        <MessageInput create={createMessage}></MessageInput>
      )}
    </>
  );
};

export default Chat;
